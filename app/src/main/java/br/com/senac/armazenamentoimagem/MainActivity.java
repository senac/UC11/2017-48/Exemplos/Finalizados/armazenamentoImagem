package br.com.senac.armazenamentoimagem;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final int CAPTURA_IMAGEM = 1  ;
    private String nomeArquivo ;
    private ImageView imageView  ;
    private TextView textView ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.foto) ;
        textView = findViewById(R.id.caminho);

    }


    public void pegarImagem(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String caminhoDoArquivoExterno = Environment.getExternalStorageDirectory().toString() ;
        String caminhoDoArquivoDentroApp = getApplication().getFilesDir().toString() ;

        Log.i("#FILE" , caminhoDoArquivoExterno) ;
        Log.i("#FILE" , caminhoDoArquivoDentroApp) ;

        // /storage/sdcard/465464546.png
       nomeArquivo = caminhoDoArquivoExterno
                + "/" + System.currentTimeMillis() + ".png";

        File arquivo = new File(nomeArquivo) ;

        Uri localDeArmazenamento = Uri.fromFile( arquivo ) ;

        intent.putExtra(MediaStore.EXTRA_OUTPUT , localDeArmazenamento ) ;



        startActivityForResult(intent , CAPTURA_IMAGEM);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(CAPTURA_IMAGEM == requestCode){
            if(resultCode == RESULT_OK){

                textView.setText(nomeArquivo);

                Bitmap fotoOriginal = BitmapFactory.decodeFile(nomeArquivo) ;

                Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoOriginal , 200 , 200 , true);

                imageView.setImageBitmap(fotoReduzida);


            }
        }

    }
















}
